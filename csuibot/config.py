from os import environ


APP_ENV = environ.get('APP_ENV', 'development')
DEBUG = environ.get('DEBUG', 'true') == 'true'
TELEGRAM_BOT_TOKEN = environ.get('TELEGRAM_BOT_TOKEN',
                                 '314746202:AAEVuPaByczzY0OjJaezI8FTmG91B6OENYk')
LOG_LEVEL = environ.get('LOG_LEVEL', 'DEBUG')
WEBHOOK_HOST = environ.get('WEBHOOK_HOST', 'https://zodiacshiobot.herokuapp.com/bot')
WEBHOOK_URL = environ.get('WEBHOOK_URL', 'https://zodiacshiobot.herokuapp.com/')
